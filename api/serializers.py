from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from course.models import Feedback, Course
from course.models import Feedback

User = get_user_model()


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ('comment',)


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('title', 'description')


class CourseFullSerializer(CourseSerializer):
    feedback = FeedbackSerializer()

    class Meta:
        model = Course
        fields = (
            *CourseSerializer.Meta.fields,
            'feedback'
        )
