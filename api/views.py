from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.serializers import CourseFullSerializer, CourseSerializer
from course.models import Course


@api_view(['GET'])
def test_api_view(request):
    """For test rest API"""
    return Response({'status': 'ok'})


class CourseAuthPermission(BasePermission):
    """Permission for auth user"""
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return True


class CourseViewSet(viewsets.ModelViewSet):
    """Courses"""
    queryset = Course.objects.select_related('feedback')
    permission_classes = [CourseAuthPermission]
    pagination_class = LimitOffsetPagination
    serializer_class = CourseSerializer

    # def get_serializer(self):
    #     if self.action == 'retrieve':
    #         return CourseFullSerializer
    #     return CourseSerializer
