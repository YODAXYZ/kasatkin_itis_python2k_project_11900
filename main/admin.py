from django.contrib import admin
from main import models


class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'get_user_email')
    list_display_links = ('id', 'first_name', 'last_name', 'get_user_email')
    ordering = ('-updated_at',)
    search_fields = ('id', 'first_name', 'last_name', 'get_user_email')
    list_filter = ('updated_at', 'gender')

    def get_queryset(self, request):
        qs = super(PersonAdmin, self).get_queryset(request)
        return qs.select_related("user")

    def get_user_email(self, instance: models.Person):
        return instance.user.email

    get_user_email.short_description = 'Почта пользователя'
    get_user_email.admin_order_field = 'user__email'


admin.site.register(models.Person, PersonAdmin)
admin.site.register(models.InstituteAdmin)
admin.site.register(models.EducationalInstitution)