from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Person(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=120, null=False, blank=False)
    last_name = models.CharField(max_length=120, null=False, blank=False)
    position = models.TextChoices('Учитель', 'Ученик')
    image = models.ImageField(upload_to='media/img/avatar', null=True, blank=True)
    gender = models.BooleanField()  # True man False girl
    birthday = models.DateField(null=False, blank=False)
    about_yourself = models.TextField(null=True, blank=True)
    score = models.IntegerField(default=0, editable=False, blank=True, null=True)

    def __str__(self):
        return self.first_name + "-" + self.last_name

    class Meta:
        verbose_name = 'Пользовавтель'
        verbose_name_plural = 'Пользователи'


class EducationalInstitution(BaseModel):
    name = models.CharField(max_length=200, null=False)
    image = models.ImageField(upload_to='media/img/education_institute', null=True, blank=True)
    address = models.CharField(max_length=300, null=False)
    site = models.URLField(null=True, blank=True)
    description = models.TextField(null=False)


class InstituteAdmin(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    institute = models.ForeignKey(EducationalInstitution, on_delete=models.CASCADE)


class PersonCreator(BaseModel):
    user = models.ForeignKey(Person, models.CASCADE)
    description = models.TextField(null=True)
    work_place = models.ForeignKey(EducationalInstitution, models.CASCADE)

