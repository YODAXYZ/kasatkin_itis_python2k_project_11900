from django.contrib.auth import authenticate, login, REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, resolve_url, redirect
from django.template import RequestContext
from django.urls import reverse_lazy, reverse
from gunicorn.config import User

from main.forms import PersonCreationForm, AuthForm
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView, FormView

from main.models import Person


class IndexPageView(TemplateView):
    template_name = 'index.html'


class RegisterView(FormView):
    form_class = UserCreationForm

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username')
        raw_password = form.cleaned_data.get('password1')
        login(self.request, authenticate(username=username, password=raw_password))
        return super(RegisterView, self).form_valid(form)


def auth_page(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, email=username, password=password)
            if user is None:
                form.add_error('username', 'Неправильный логин или пароль')
            else:
                login(request, user)
                return redirect('index')

    return render(request, 'auth/login.html', {
        'form': form
    })



@login_required(login_url='login')
def create_profile(request):
    try:
        person = request.user.person
        form = PersonCreationForm(instance=person)
    except:
        form = PersonCreationForm()

    if request.method == 'POST':
        form = PersonCreationForm(request.POST, request.FILES)
        if form.is_valid():
            print(form)
            # form.data['user'] = request.user
            # form.user_id = request.user.id
            # form.save()
            # print('----------')
            # print(form.data)
            # print('-----')
            new_form = form.data.copy()
            if new_form['gender'] == 'on':
                new_form['gender'] = True
            else:
                new_form['gender'] = False
            obj, created = Person.objects.update_or_create(user=request.user,defaults=new_form)
            return render(request, 'index.html')


    # if request.method == 'POST':
    #     form = PersonCreationForm(request.POST, request.FILES)
    #     if form.is_valid():
    #         form.user_id = request.user.id
    #         form.save()
    else:
        return render(request, 'auth/profile-create.html', {'form': form})


# class ContactsView(TemplateView):
#     template_name = 'contact.html'
#
#
# class AboutUsVies(TemplateView):
#     template_name = 'about-us.html'
#
#
# class PetListView(ListView):
#     template_name = 'pets/pet-list.html'
#     model = Animal
#
#
# class PetView(DetailView):
#     template_name = 'pets/pet-deteil.html'
#     model = Animal




