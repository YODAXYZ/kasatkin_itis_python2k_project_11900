from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, reverse_lazy
from main import views

urlpatterns = [
    path('', views.IndexPageView.as_view(), name='index'),
    path('login/', LoginView.as_view(template_name='auth/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', views.RegisterView.as_view(
        template_name='auth/register.html',
        success_url=reverse_lazy('profile-create')
    ), name='register'),
    path('profile-create/', views.create_profile, name='profile-create'),
]