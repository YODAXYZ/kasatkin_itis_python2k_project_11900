from django import forms
from django.contrib.auth.forms import UserCreationForm
from main.models import Person, User


class PersonCreationForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = '__all__'
        exclude = ['user', 'scope']

# class UserCreationForm(UserCreationForm):
#     email = forms.EmailField(required=True)
#
#     class Meta:
#         model = User
#         fields = ("username", "email", "password1", "password2")
#
#     def clean(self):
#         cleaned_data = super().clean()
#         if cleaned_data['password1'] != cleaned_data['password2']:
#             self.add_error('password', 'Пароли не совпадают')
#         return cleaned_data
#
#     def save(self, commit=True):
#         user = super(UserCreationForm, self).save(commit=False)
#         user.email = self.cleaned_data["email"]
#         if commit:
#             user.save()
#         return user

class AuthForm(forms.Form):
    username = forms.CharField(label='UserName')
    password = forms.CharField(label='Password')
