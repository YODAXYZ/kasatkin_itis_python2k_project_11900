# Generated by Django 3.1.7 on 2021-04-05 19:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20210405_1438'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='under_comm',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='user',
        ),
        migrations.RemoveField(
            model_name='course',
            name='feedback',
        ),
        migrations.RemoveField(
            model_name='feedback',
            name='under_feed',
        ),
        migrations.RemoveField(
            model_name='feedback',
            name='user',
        ),
        migrations.DeleteModel(
            name='ScoreRating',
        ),
        migrations.RemoveField(
            model_name='task',
            name='comment',
        ),
        migrations.RemoveField(
            model_name='task',
            name='course',
        ),
        migrations.RemoveField(
            model_name='undercomment',
            name='user',
        ),
        migrations.RemoveField(
            model_name='underfeedback',
            name='user',
        ),
        migrations.DeleteModel(
            name='Comment',
        ),
        migrations.DeleteModel(
            name='Course',
        ),
        migrations.DeleteModel(
            name='Feedback',
        ),
        migrations.DeleteModel(
            name='Task',
        ),
        migrations.DeleteModel(
            name='UnderComment',
        ),
        migrations.DeleteModel(
            name='UnderFeedback',
        ),
    ]
