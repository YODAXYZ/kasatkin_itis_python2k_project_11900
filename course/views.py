from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from course.models import Course


class CourseListView(ListView):
    template_name = 'course/course-list.html'
    model = Course
    paginate_by = 20


class CourseDetailView(DetailView):
    template_name = 'course/course-detail.html'
    model = Course

