from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from main.models import BaseModel, Person, PersonCreator


class UnderComment(BaseModel):
    user = models.ForeignKey(Person, models.CASCADE)
    comment_message = models.TextField(null=False)


class Comment(BaseModel):
    user = models.ForeignKey(Person, models.CASCADE)
    comment_message = models.TextField(null=False)
    under_comm = models.ForeignKey(UnderComment, models.CASCADE, null=True, blank=True)


class UnderFeedback(BaseModel):
    user = models.ForeignKey(Person, models.CASCADE)
    comment = models.TextField(null=False)


class Feedback(BaseModel):
    user = models.ForeignKey(Person, on_delete=models.CASCADE)
    comment = models.TextField(null=False)
    validators = models.FloatField(validators=[MaxValueValidator(1), MinValueValidator(5)])
    under_feed = models.ForeignKey(UnderFeedback, on_delete=models.CASCADE)


class Course(BaseModel):
    title = models.CharField(max_length=200, null=False)
    description = models.TextField(null=False)
    creator = models.ManyToManyRel(PersonCreator, to=models.Model)  # нужна еще возможность добавлять курс от школы
    participant = models.ManyToManyRel(Person, to=models.Model)
    image = models.ImageField(upload_to='img/course', null=True, blank=True)
    feedback = models.ForeignKey(Feedback, on_delete=models.CASCADE, null=True, blank=True)


class Task(BaseModel):
    title = models.CharField(max_length=300, null=False)
    position = models.IntegerField()
    description = models.TextField(max_length=300, null=False)
    # type_of_task = models.TextChoices('Ответ с выборки', 'Ответ текстовый',
    # 'Без задания', 'С проверкой преподоввателя')
    count_of_sample = models.IntegerField(default=999, null=False)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=True, blank=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True, blank=True)


class ScoreRating(BaseModel):
    user = models.ManyToManyRel(Person, to=models.Model)
    score = models.IntegerField(default=0, editable=False, blank=True)
    course = models.ManyToManyRel(Course, to=models.Model)
