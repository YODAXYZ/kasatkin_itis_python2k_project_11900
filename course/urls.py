from django.urls import path, re_path
from course import views

app_name = 'course'
urlpatterns = [
    path('courses/', views.CourseListView.as_view(), name='course-list'),
    # path('course/<int:pk>', views.CourseDetailView.as_view(), name='course-detail')  # nice
    re_path(r'^course/(?P<pk>[0-9]+)/$', views.CourseDetailView.as_view(), name='course-detail') # trash

]